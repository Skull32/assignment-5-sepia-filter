GCC = gcc
GCC_FLAGS = -no-pie
ASM = nasm
ASM_FLAGS = -felf64
SRC_DIR = ./solution/src/
BUILD_DIR = ./build/
OUT_DIR = ./out/

filter.o: $(SRC_DIR)filter.asm
	$(ASM) $(ASM_FLAGS) $^ -o $(BUILD_DIR)$@

sepia-filter: $(SRC_DIR)*.asm $(SRC_DIR)*.c
	$(GCC) $(SRC_DIR)*.c $(BUILD_DIR)*.o -o $(OUT_DIR)$@

clean:
	rm -rf $(OUT_DIR)*
	rm -rf $(BUILD_DIR)*
