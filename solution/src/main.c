#include "../include/bmp.h"
#include "../include/file_handler.h"
#include "../include/image_processor.h"
#include "../include/filter.h"
#include "../include/sepia_asm_filter.h"
#include "../include/image.h"
#include "../include/utils.h"
#include <time.h>
#include <stdlib.h>

#define ITERATION 100

int main(int argc, char** argv ) {
    
    FILE* in_file;
    FILE *src_file;
    
    read_file("input.bmp", &in_file);
    file_status open_file_status = read_file("input.bmp", &src_file);

    if (open_file_status != FILE_OK) {
        print_error(file_proc_status_names[open_file_status]);
        return 1;
    }

    struct image* img;
    read_code_t read_bmp_status = from_bmp(in_file, &img);

    if (read_bmp_status != READ_OK) {
        destroy_image(img);
        print_error(read_status_names[read_bmp_status]);
        return 1;
    }
    fclose(src_file);

    FILE* out_file1;
    FILE* out_file2;

    file_status open_file_status1 = write_file("output1.bmp", &out_file1);

    if (open_file_status1 != FILE_OK) {
        destroy_image(img);
        print_error(file_proc_status_names[open_file_status1]);
        return 1;
    }

    file_status open_file_status2 = write_file("output2.bmp", &out_file2);

    if (open_file_status2 != FILE_OK) {
        destroy_image(img);
        print_error(file_proc_status_names[open_file_status2]);
        return 1;
    }

    size_t iteration = ITERATION;

    struct image* out1;

    clock_t start_sepia_c = clock();
    for (size_t i = 0; i < iteration; i++)
        out1 = sepia_c(img);
    clock_t end_sepia_c = clock();

    struct image* out2;

    clock_t start_sepia_asm = clock();
    for (size_t i = 0; i < iteration; i++)
        out2 = sepia_asm(img);
    clock_t end_sepia_asm = clock();

    double c_result   = (double) (end_sepia_c - start_sepia_c) / CLOCKS_PER_SEC;
    double asm_result = (double) (end_sepia_asm - start_sepia_asm) / CLOCKS_PER_SEC;
    printf("Number of iteration: %-5zu\n", iteration);
    printf("%-11s %s \n", "Realization",  "Result");
    printf("%-11s %.2fs \n", "SIMD", asm_result);
    printf("%-11s %.2fs \n", "NATIVE C", c_result);
    printf("Acceleration: %.2f\n", c_result / asm_result);

    write_code_t write_bmp_status1 = to_bmp(out_file1, out1);

    if (write_bmp_status1 != WRITE_OK) {
        destroy_image(img);
        destroy_image(out1);
        print_error(write_status_names[write_bmp_status1]);
        return 1;
    }

    write_code_t write_bmp_status2 = to_bmp(out_file2, out2);

    if (write_bmp_status2 != WRITE_OK) {
        destroy_image(img);
        destroy_image(out2);
        print_error(write_status_names[write_bmp_status2]);
        return 1;
    }

    fclose(out_file1);
    fclose(out_file2);
    destroy_image(img);

    return 0;
}