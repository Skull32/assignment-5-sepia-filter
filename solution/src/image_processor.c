#include "../include/bmp.h"
#include "../include/image_processor.h"

#include <stdlib.h>

#define BMP_TYPE 0x4D42

char* read_status_names[5] = {
        "Reading was successful",
        "Incorrect signature read",
        "incorrect bits read",
        "incorrect head read",
        "Not enough memory to read"
};


char* write_status_names[2] = {
        "Writing was successful",
        "An error occurred while writing the file"
};

read_code_t read_bmp_header(FILE* input, struct bmp_header* bmp_header_read) {
    const size_t header_size = STRUCT_BMP_HEADER_SIZE - sizeof(uint16_t);
    const size_t signature_read = fread(&bmp_header_read->bfType, STRUCT_BMP_HEADER_SIZE, 1, input);
    const size_t header_read = fread(bmp_header_read + sizeof(uint16_t), header_size, 1, input);

    if (signature_read != 1 || bmp_header_read->bfType != BMP_TYPE)
        return READ_INVALID_SIGNATURE;
    if (header_read != 1)
        return READ_INVALID_HEADER;

    return READ_OK;
}

read_code_t from_bmp(FILE* input, struct image** img) {
    struct bmp_header header;
//    read_code_t read = read_bmp_header(input, &header);
//    if (read != READ_OK) return read;

    (*img) = malloc(STRUCT_IMAGE_SIZE);
    if (*img == NULL) {
        free(img);
        return READ_NO_ENOUGH_MEMORY;
    }

    if (fread(&header, STRUCT_BMP_HEADER_SIZE, 1, input) < 1)
        return READ_INVALID_HEADER;
    if (header.bfType != BMP_TYPE)
        return READ_INVALID_HEADER;

    (*img)->width = (uint64_t) header.biWidth;
    (*img)->height = (uint64_t) header.biHeight;

    (*img)->data = malloc(STRUCT_PIXEL_SIZE * (*img)->width * (*img)->height);
    if ((*img)->data == NULL) {
        free((*img)->data);
        return READ_NO_ENOUGH_MEMORY;
    }

    uint8_t padding = calc_padding((*img)->width);

    fseek(input, (long) header.bOffBits, SEEK_SET);
    size_t curr_pixel = 0;

    for (size_t i = 0; i < (*img)->height; i++) {
        for (size_t j = 0; j < (*img)->width; j++) {
            uint8_t colours[STRUCT_PIXEL_SIZE] = {0};
            if (fread(&colours, sizeof(uint8_t), STRUCT_PIXEL_SIZE, input) < STRUCT_PIXEL_SIZE) {
                return READ_INVALID_BITS;
            }

            (*img)->data[curr_pixel++] = (struct pixel) {
                    .b = colours[0],
                    .g = colours[1],
                    .r = colours[2]
            };
        }
        fseek(input, (long) padding, SEEK_CUR);
    }
    return READ_OK;
}

write_code_t write_bmp_header(FILE* out, struct bmp_header const* bmp_header_write) {
    if (fwrite(bmp_header_write, STRUCT_BMP_HEADER_SIZE, 1, out) != 1)
        return WRITE_HEADER_ERROR;
    return WRITE_OK;
}

write_code_t to_bmp(FILE* out, struct image const* img) {
    struct bmp_header header = create_bmp_header(img);

    write_code_t write_header = write_bmp_header(out, &header);
    if (write_header != WRITE_OK) return write_header;

    uint32_t padding = calc_padding(img->width);
    struct pixel* current_row;
    size_t row_size = img->width * STRUCT_PIXEL_SIZE;

    for (size_t i = 0; i < img->height; i++) {
        current_row = img->data + i * (img->width);

        if (fwrite(current_row, row_size, 1, out) != 1)
            return WRITE_IMAGE_ERROR;

        uint32_t zero_padding = 0;

        if (fwrite(&zero_padding, sizeof(uint8_t), padding, out) != padding)
            return WRITE_PADDING_ERROR;
    }

    return WRITE_OK;
}