%define PIXEL_SIZE 3
%define MAX_COLOR 255

section .rodata
align 16
max_values_index: dd MAX_COLOR, MAX_COLOR, MAX_COLOR, 0
r_index: dd 0.272, 0.349, 0.393, 0.0
g_index: dd 0.534, 0.686, 0.769, 0.0
b_index: dd 0.131, 0.168, 0.189, 0.0

%macro pack_to_xmm 2    ; (xmm, pixel color ptr)
    xorps  %1, %1
    pinsrb %1, [%2], 0
    pinsrb %1, [%2], 4
    pinsrb %1, [%2], 8
    cvtdq2ps %1, %1
%endmacro

section .text
global sepia_asm_filter

sepia_asm_filter:
    mov rbx, PIXEL_SIZE
    mov rax, rdi
    mov r12, rax
    mov r13, rsi
    mov r14, rdx

    mul r13
    mul rbx
    mov r15, rax

    movaps xmm3, [rel b_index]
    movaps xmm4, [rel g_index]
    movaps xmm5, [rel r_index]
    movaps xmm6, [rel max_values_index]
    mov rbx, r14
.loop:
    pack_to_xmm xmm0, rbx
    pack_to_xmm xmm1, rbx + 1
    pack_to_xmm xmm2, rbx + 2
    mulps xmm0, xmm3
    mulps xmm1, xmm4
    mulps xmm2, xmm5
    addps xmm0, xmm1
    addps xmm0, xmm2
    cvtps2dq xmm0, xmm0
    pminud xmm0, xmm6
    pextrb [rbx], xmm0, 0
    pextrb [rbx + 1], xmm0, 4
    pextrb [rbx + 2], xmm0, 8

    add rbx, PIXEL_SIZE
    sub r15, PIXEL_SIZE
    cmp r15, 0
    jg .loop
    ret