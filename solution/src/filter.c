#include "../include/bmp.h"
#include "../include/sepia_asm_filter.h"

#define MAX_PIXEL_VALUE 255
#define RR  0.393f
#define RG  0.769f
#define RB  0.189f
#define GR  0.349f
#define GG  0.686f
#define GB  0.168f
#define BR  0.272f
#define BG  0.534f
#define BB  0.131f

static uint8_t valid_pixel(uint64_t pixel_value) {
    return pixel_value > MAX_PIXEL_VALUE ? MAX_PIXEL_VALUE : pixel_value;
}

void sepia_pixel(struct pixel *const pixel) {
    uint8_t r = pixel->r;
    uint8_t g = pixel->g;
    uint8_t b = pixel->b;

    pixel->r = valid_pixel((r * RR) + (g * RG) + (b * RB));
    pixel->g = valid_pixel((r * GR) + (g * GG) + (b * GB));
    pixel->b = valid_pixel((r * BR) + (g * BG) + (b * BB));
}

struct image* sepia_c(struct image* img) {
    struct image* out = copy_image(img);

    for (size_t i = 0; i < out->height * out->width; i++) {
        sepia_pixel(&(out->data[i]));
    }
    return out;
}

struct image* sepia_asm(struct image* img) {
    struct image* out = copy_image(img);
    sepia_asm_filter(out->height, out->width, out->data);
    return out;
}