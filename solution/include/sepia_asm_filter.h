#ifndef SEPIA_FILTER_H
#define SEPIA_FILTER_H

#include "bmp.h"
#include <stdint.h>

void sepia_asm_filter(uint64_t height, uint64_t width, struct pixel* data);

#endif //SEPIA_FILTER_H