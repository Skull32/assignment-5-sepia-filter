
#ifndef BMP_H
#define BMP_H

#include "image.h"
#include "pixel.h"
#include <stdint.h>
#include <stdio.h>

#define STRUCT_BMP_HEADER_SIZE sizeof(struct bmp_header)

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

uint32_t calc_padding(uint32_t width);

struct bmp_header create_bmp_header(struct image const* img);

#endif //BMP_H