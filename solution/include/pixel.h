#ifndef PIXEL_H
#define PIXEL_H

#include <stdint.h>

#define STRUCT_PIXEL_SIZE sizeof(struct pixel)

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

#endif //PIXEL_H