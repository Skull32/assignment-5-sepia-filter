#ifndef IMAGE_H
#define IMAGE_H

#include "pixel.h"
#include <stdint.h>

#define STRUCT_IMAGE_SIZE sizeof(struct image)

struct image {
    uint64_t width, height;
    struct pixel* data;
};

void destroy_image(struct image* image);

void destroy_image_memory(struct image* image);

struct image *create_image(uint64_t width, uint64_t height);

struct image* copy_image(struct image* source);

#endif //IMAGE_H