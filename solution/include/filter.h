#ifndef FILTER_H
#define FILTER_H

#include "bmp.h"

struct image* sepia_c(struct image* img);
struct image* sepia_asm(struct image* img);

#endif //FILTER_H