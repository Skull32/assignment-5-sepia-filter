#ifndef IMAGE_PROCESSOR_H
#define IMAGE_PROCESSOR_H

#include "image.h"
#include "stdio.h"

extern char* read_status_names[5];
extern char* write_status_names[2];

typedef enum write_code_t{
    WRITE_OK = 0,
    WRITE_IMAGE_ERROR,
    WRITE_PADDING_ERROR,
    WRITE_HEADER_ERROR
} write_code_t;

typedef enum read_code_t{
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_NO_ENOUGH_MEMORY
} read_code_t;

read_code_t from_bmp(FILE* input, struct image** img);
write_code_t to_bmp(FILE* out, struct image const* img);

#endif //IMAGE_PROCESSOR_H